#!/usr/bin/env bash

# Variables
DBHOST=localhost
DBNAME=tree_manager
DBUSER=shiro
DBPASSWORD=audrino

    mysql -e "GRANT ALL PRIVILEGES ON tree_manager.* TO 'shiro'@'audrino'"

yum install -y mariadb-server
systemctl start mariadb
systemctl enable mariadb
systemctl status mariadb

# >> /vagrant/vm_build.log 2>&1

mysql -u root -e "DROP DATABASE IF EXISTS $DBNAME"
mysql -u root -e "CREATE DATABASE $DBNAME DEFAULT CHARACTER SET utf8"

if ! mysql -u root -e "SELECT user FROM mysql.user WHERE user='$DBUSER'"; then
    mysql -u root -e "CREATE USER '$DBUSER'@'$DBHOST' IDENTIFIED BY '$DBPASSWORD'"
    mysql -u root -e "GRANT ALL PRIVILEGES ON $DBNAME.* TO '$DBUSER'@'$DBHOST'"
    mysql -u root -e "FLUSH PRIVILEGES"
fi

mysql $DBNAME < /vagrant/sql/01-schema.sql

sudo yum install -y vim
sudo yum install -y gcc
sudo yum install -y perl
sudo yum install -y perl-core
sudo yum install -y perl-devel
sudo yum install -y perl-App-cpanminus
sudo yum install -y perl-Clone
sudo yum install -y perl-Template-Toolkit
sudo yum install -y perl-DBIx-Class-Schema-Loader
sudo yum install -y perl-SQL-Translator
sudo yum install -y perl-Test-API

rpm -Uvh http://repo.openfusion.net/centos7-x86_64/openfusion-release-0.7-1.of.el7.noarch.rpm
yum install -y perl-Hash-Merge-Simple

#sudo yum -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
#sudo yum -y --enablerepo=extras install epel-release

#wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
#wget https://rpms.remirepo.net/enterprise/remi-release-7.rpm
rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
rpm -Uvh https://rpms.remirepo.net/enterprise/remi-release-7.rpm

cpanm -S Dancer2
#cpanm -S Test::Modern
#cpanm -S DBICx::Sugar
#cpanm -S Dancer2::Plugin::DBIC