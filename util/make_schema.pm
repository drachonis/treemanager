#!/usr/bin/env perl

use strict;
use warnings;
use DBIx::Class::Schema::Loader qw (make_schema_at);

make_schema_at(
    'App::Schema',
    {
        debug => 1,
        dump_directory => '../Tree-Manager/lib'
    },
    [ "dbi:mysql:tree_manager", 'root', '', { RaiseError => 1,} ]
);
