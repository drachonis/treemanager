DROP TABLE IF EXISTS `tree`;
DROP TABLE IF EXISTS `node`;
DROP TABLE IF EXISTS `edge`;
DROP TABLE IF EXISTS `root_node`;
DROP TABLE IF EXISTS `db_version`;

-- group of tree
CREATE TABLE `tree` (
  `tree_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`tree_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Group of Tree';

-- node of tree
CREATE TABLE `node` (
  `node_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tree_id` INT(10) UNSIGNED NOT NULL COMMENT 'The tree',
  `depth` INT(10) UNSIGNED NOT NULL COMMENT 'Depth of the node',
  PRIMARY KEY (`node_id`, `tree_id`),
  CONSTRAINT `node_tree_id` FOREIGN KEY (`tree_id`) REFERENCES `tree` (`tree_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Node of Tree';

-- root node of tree
CREATE TABLE `root_node` (
  `tree_id` INT(10) UNSIGNED NOT NULL,
  `node_id` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`tree_id`, `node_id`),
  CONSTRAINT `root_node_tree_id` FOREIGN KEY (`tree_id`) REFERENCES `tree` (`tree_id`),
  CONSTRAINT `root_node_node_id` FOREIGN KEY (`node_id`) REFERENCES `node` (`node_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Root node of tree';

-- edge of tree
CREATE TABLE `edge` (
  `edge_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tree_id` INT(10) UNSIGNED NOT NULL,
  `parent_node_id` INT(10) UNSIGNED NOT NULL,
  `child_node_id` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`edge_id`),
  CONSTRAINT `edge_tree_id` FOREIGN KEY (`tree_id`) REFERENCES `tree` (`tree_id`),
  CONSTRAINT `edge_parent_node_id` FOREIGN KEY (`parent_node_id`) REFERENCES `node` (`node_id`),
  CONSTRAINT `edge_child_node_id` FOREIGN KEY (`child_node_id`) REFERENCES `node` (`node_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Edge of tree';

-- version of database
CREATE TABLE `db_version` (
  `db_version_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tag` VARCHAR(64) NOT NULL COMMENT 'Unique tag',
  `note` VARCHAR(256) NOT NULL COMMENT 'Note what is done',
  PRIMARY KEY (`db_version_id`),
  CONSTRAINT `db_version_tag` UNIQUE KEY (`tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Version of database';

INSERT INTO `db_version` (`tag`, `note`) VALUES ('Initialization', 'Initialization of new database and tables');
