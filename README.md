Tree Manager
=======

Prerequisites
===

Plugin for syncing folders:

```
$ vagrant plugin install vagrant-vbguest
```

Vagrant box is preferred way of running Tree Manager application. It comes with Vagrantfile a provisioning script.
Enter into the application directory and run:

```
$ vagrant up
```

Box start downloading the image of CentOS 7 and provisioning. Provisioning install necessary software:
    - Perl and Perl modules
    - MariaDB

Also create a setup database of tree manager application. Check database:
```
$ vagrant ssh
$ mysql -u shiro -paudrino tree_manager
```

In Vagrant Box
===

Run the application from command line:
```
$ vagrant ssh -c 'plackup -p 5000 /vagrant/Tree-Manager/bin/app.psgi' -- -L 5000:localhost:5000
```

It is based a PSGI application.

View the web application at:
http://localhost:5000

Out side of Vagrant box
===
From your distribution directory, run:
```
plackup -p 5000 ./Tree-Manager/bin/app.psgi
```

Application design is very spartan Dancer/Bootstrap template.

Structure of application copy idea of micro framework Dancer2. The base there are routes and views templates.

Routes:

   - GET /tree - Viewing all trees with possible action make new one
   - GET /tree/:id - Viewing current tree with possible action add new node
   - POST /create - Create new tree
   - POST /add - Create new node to current tree

First thought was to use the DBIx in Dancer2 framework, but there is no package available via system package manager yum.
I thinking of changing the linux distribution from CentOS to Ubuntu, but don't have time to do it. It take a lot of time
to provisioning the vagrant box to the current state.

Database schema is generated via  DBIx::Class::Schema::Loader.

Next step:

Enhance the default layout with some boostrap template. Adding login and logout user into application. Adding log errors.
Creating controllers and models that will separate login from routes. Format code with Perl Tidy. Change provisioning,
change distribution box, or install perl via brew perl.

