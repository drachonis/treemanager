package Tree::Manager;
use Dancer2;
use DBI;
# Don't used it, vagrant box cannot be too much
#use Dancer2::Plugin::DBIC qw(schema resultset rset);

our $VERSION = '0.1';

sub set_flash {
    my $message = shift;
    session flash => $message;
}

sub get_flash {
    my $msg = session('flash');
    session->delete('flash');
    return $msg;
}

sub connect_db {
    my $dbh = DBI->connect(
        "dbi:mysql:tree_manager",
        "shiro",
        "audrino",
        { RaiseError => 1, AutoCommit => 0 }
    ) or
        die $DBI::errstr;

    return $dbh;
}

sub create_new_tree {
    # create tree
    my $dbh = connect_db();
    my $sql = 'INSERT INTO `tree`() VALUES ()';
    my $sth = $dbh->prepare($sql) or die $dbh->errstr;
    $sth->execute() or die $sth->errstr;

    my $tree_id = $dbh->last_insert_id(undef, 'tree_manager', 'tree', 'tree_id');

    # create node
    my $sql_node = 'INSERT INTO `node` (`tree_id`, `depth`) VALUES (?, 0)';
    my $sth_node = $dbh->prepare($sql_node) or die $dbh->errstr;
    $sth_node->execute($tree_id) or die $sth->errstr;

    my $node_id = $dbh->last_insert_id(undef, 'tree_manager', 'node', 'node_id');

    # create root node
    my $sql_root = 'INSERT INTO `root_node` (`tree_id`, `node_id`) VALUES (?, ?)';
    my $sth_root = $dbh->prepare($sql_root) or die $dbh->errstr;
    $sth_root->execute($tree_id, $node_id) or die $dbh->errstr;

    # we are in the transaction mode
    $dbh->commit();
}

sub load_trees {

    my $dbh = connect_db();
    my $sql = 'SELECT `tree_id` FROM `tree` ORDER BY `tree_id` DESC';
    my $sth = $dbh->prepare($sql) or die $dbh->errstr;
    $sth->execute or die $sth->errstr;
    return $sth->fetchall_arrayref();
}


get '/' => sub {
    template 'index' => { 'title' => 'Tree::Manager' };
};

# Overview of trees
get '/tree' => sub {

    my $tree = load_trees();

    template 'tree' => {
        'title'           => 'Tree::Manager',
        'tree'            => $tree,
        'msg'             => get_flash(),
        'create_tree_url' => uri_for('/create'),
        'uri_for_tree'    => uri_for('/tree'),
    };
};

# Current tree
get '/tree/:id' => sub {

    my $tree_id = route_parameters->get('id');
    my $dbh = connect_db();
    my $sql = '
        SELECT
            `node`.`node_id`,
            `node`.`depth`,
            `edge`.`parent_node_id`
        FROM
            `node`
        LEFT JOIN
            `edge`
            ON (`edge`.`child_node_id` = `node`.`node_id`)
        WHERE
            `node`.`tree_id` = ?
        ORDER BY
            `node`.`node_id` DESC';
    my $sth = $dbh->prepare($sql) or die $dbh->errstr;
    $sth->execute($tree_id) or die $sth->errstr;

    my $node = $sth->fetchall_hashref(['depth', 'parent_node_id', 'node_id']);

    template 'current_tree' => {
        'title'         => 'Current::Tree',
        'add_node_url'  => uri_for('/add'),
        'node'          => $node,
        'tree_id'       => $tree_id,
        'msg'           => get_flash(),
    };
};

# Add node to tree
post '/add' => sub {
    my $id = body_parameters->get('pid');
    my $tree_id = body_parameters->get('tree_id');

    my $dbh = connect_db();
    my $sql = 'SELECT `node_id`, `tree_id`, `depth` FROM `node` WHERE `node_id` = ? AND `tree_id` = ?';
    my $sth = $dbh->prepare($sql) or die $dbh->errstr;
    $sth->execute($id, $tree_id);
    my $node = $sth->fetchrow_hashref();
    if ($node->{node_id}) {
        #add node
        my $sql_add = 'INSERT INTO `node` (`tree_id`, `depth`) VALUES (?, ?)';
        my $sth_add = $dbh->prepare($sql_add) or die $dbh->errstr;
        my $new_depth = $node->{depth} + 1;
        $sth_add->execute($node->{tree_id}, $new_depth);

        my $child_id = $dbh->last_insert_id(undef, 'tree_manager', 'node', 'node_id');

        #add edge
        my $sql_edge = 'INSERT INTO `edge` (`tree_id`, `parent_node_id`, `child_node_id`) VALUES (?, ?, ?)';
        my $sth_edge = $dbh->prepare($sql_edge) or die $dbh->errstr;
        $sth_edge->execute($node->{tree_id}, $node->{node_id}, $child_id);

        $dbh->commit();
        set_flash('New node added');
    } else {
        set_flash('Parent node not found!');
    }

    redirect join('/', '/tree', $tree_id) ;

};


# Create tree with root node
post '/create' => sub {

    create_new_tree();
    set_flash('Tree created');
    redirect '/tree';
};

true;
