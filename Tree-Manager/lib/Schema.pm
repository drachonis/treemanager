use utf8;
package App::Schema;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Schema';

__PACKAGE__->load_namespaces;


# Created by DBIx::Class::Schema::Loader v0.07040 @ 2019-07-10 07:23:16
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:kjpmSeF3RwwrbTXzcKQlhg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
