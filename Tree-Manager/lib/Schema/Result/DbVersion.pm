use utf8;
package App::Schema::Result::DbVersion;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

App::Schema::Result::DbVersion - Version of database

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<db_version>

=cut

__PACKAGE__->table("db_version");

=head1 ACCESSORS

=head2 db_version_id

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 tag

  data_type: 'varchar'
  is_nullable: 0
  size: 64

Unique tag

=head2 note

  data_type: 'varchar'
  is_nullable: 0
  size: 256

Note what is done

=cut

__PACKAGE__->add_columns(
  "db_version_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "tag",
  { data_type => "varchar", is_nullable => 0, size => 64 },
  "note",
  { data_type => "varchar", is_nullable => 0, size => 256 },
);

=head1 PRIMARY KEY

=over 4

=item * L</db_version_id>

=back

=cut

__PACKAGE__->set_primary_key("db_version_id");

=head1 UNIQUE CONSTRAINTS

=head2 C<db_version_tag>

=over 4

=item * L</tag>

=back

=cut

__PACKAGE__->add_unique_constraint("db_version_tag", ["tag"]);


# Created by DBIx::Class::Schema::Loader v0.07040 @ 2019-07-10 07:23:16
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:LS1HmvfOjUxKj3budzKFxQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
