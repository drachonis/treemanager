use utf8;
package App::Schema::Result::Edge;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

App::Schema::Result::Edge - Edge of tree

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<edge>

=cut

__PACKAGE__->table("edge");

=head1 ACCESSORS

=head2 edge_id

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 tree_id

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 parent_node_id

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 child_node_id

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "edge_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "tree_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "parent_node_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "child_node_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</edge_id>

=back

=cut

__PACKAGE__->set_primary_key("edge_id");

=head1 RELATIONS

=head2 child_node

Type: belongs_to

Related object: L<App::Schema::Result::Node>

=cut

__PACKAGE__->belongs_to(
  "child_node",
  "App::Schema::Result::Node",
  { node_id => "child_node_id" },
  { is_deferrable => 1, on_delete => "RESTRICT", on_update => "RESTRICT" },
);

=head2 parent_node

Type: belongs_to

Related object: L<App::Schema::Result::Node>

=cut

__PACKAGE__->belongs_to(
  "parent_node",
  "App::Schema::Result::Node",
  { node_id => "parent_node_id" },
  { is_deferrable => 1, on_delete => "RESTRICT", on_update => "RESTRICT" },
);

=head2 tree

Type: belongs_to

Related object: L<App::Schema::Result::Tree>

=cut

__PACKAGE__->belongs_to(
  "tree",
  "App::Schema::Result::Tree",
  { tree_id => "tree_id" },
  { is_deferrable => 1, on_delete => "RESTRICT", on_update => "RESTRICT" },
);


# Created by DBIx::Class::Schema::Loader v0.07040 @ 2019-07-10 07:23:16
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:wFmxV25OdkMihyMGHbbu8A


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
