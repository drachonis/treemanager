use utf8;
package App::Schema::Result::Tree;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

App::Schema::Result::Tree - Group of Tree

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<tree>

=cut

__PACKAGE__->table("tree");

=head1 ACCESSORS

=head2 tree_id

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "tree_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</tree_id>

=back

=cut

__PACKAGE__->set_primary_key("tree_id");

=head1 RELATIONS

=head2 edges

Type: has_many

Related object: L<App::Schema::Result::Edge>

=cut

__PACKAGE__->has_many(
  "edges",
  "App::Schema::Result::Edge",
  { "foreign.tree_id" => "self.tree_id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 nodes_2s

Type: has_many

Related object: L<App::Schema::Result::Node>

=cut

__PACKAGE__->has_many(
  "nodes_2s",
  "App::Schema::Result::Node",
  { "foreign.tree_id" => "self.tree_id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 root_nodes

Type: has_many

Related object: L<App::Schema::Result::RootNode>

=cut

__PACKAGE__->has_many(
  "root_nodes",
  "App::Schema::Result::RootNode",
  { "foreign.tree_id" => "self.tree_id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 nodes

Type: many_to_many

Composing rels: L</root_nodes> -> node

=cut

__PACKAGE__->many_to_many("nodes", "root_nodes", "node");


# Created by DBIx::Class::Schema::Loader v0.07040 @ 2019-07-10 07:23:16
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:3P6xBUkggokstTfg/grvzQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
