use utf8;
package App::Schema::Result::RootNode;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

App::Schema::Result::RootNode - Root node of tree

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<root_node>

=cut

__PACKAGE__->table("root_node");

=head1 ACCESSORS

=head2 tree_id

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=head2 node_id

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "tree_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "node_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</tree_id>

=item * L</node_id>

=back

=cut

__PACKAGE__->set_primary_key("tree_id", "node_id");

=head1 RELATIONS

=head2 node

Type: belongs_to

Related object: L<App::Schema::Result::Node>

=cut

__PACKAGE__->belongs_to(
  "node",
  "App::Schema::Result::Node",
  { node_id => "node_id" },
  { is_deferrable => 1, on_delete => "RESTRICT", on_update => "RESTRICT" },
);

=head2 tree

Type: belongs_to

Related object: L<App::Schema::Result::Tree>

=cut

__PACKAGE__->belongs_to(
  "tree",
  "App::Schema::Result::Tree",
  { tree_id => "tree_id" },
  { is_deferrable => 1, on_delete => "RESTRICT", on_update => "RESTRICT" },
);


# Created by DBIx::Class::Schema::Loader v0.07040 @ 2019-07-10 07:23:16
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:im+tsSPMd21t+nIeQgiYIg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
