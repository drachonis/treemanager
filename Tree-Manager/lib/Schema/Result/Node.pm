use utf8;
package App::Schema::Result::Node;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

App::Schema::Result::Node - Node of Tree

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<node>

=cut

__PACKAGE__->table("node");

=head1 ACCESSORS

=head2 node_id

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 tree_id

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

The tree

=head2 depth

  data_type: 'integer'
  extra: {unsigned => 1}
  is_nullable: 0

Depth of the node

=cut

__PACKAGE__->add_columns(
  "node_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "tree_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "depth",
  { data_type => "integer", extra => { unsigned => 1 }, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</node_id>

=item * L</tree_id>

=back

=cut

__PACKAGE__->set_primary_key("node_id", "tree_id");

=head1 RELATIONS

=head2 edge_child_nodes

Type: has_many

Related object: L<App::Schema::Result::Edge>

=cut

__PACKAGE__->has_many(
  "edge_child_nodes",
  "App::Schema::Result::Edge",
  { "foreign.child_node_id" => "self.node_id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 edge_parent_nodes

Type: has_many

Related object: L<App::Schema::Result::Edge>

=cut

__PACKAGE__->has_many(
  "edge_parent_nodes",
  "App::Schema::Result::Edge",
  { "foreign.parent_node_id" => "self.node_id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 root_nodes

Type: has_many

Related object: L<App::Schema::Result::RootNode>

=cut

__PACKAGE__->has_many(
  "root_nodes",
  "App::Schema::Result::RootNode",
  { "foreign.node_id" => "self.node_id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 tree

Type: belongs_to

Related object: L<App::Schema::Result::Tree>

=cut

__PACKAGE__->belongs_to(
  "tree",
  "App::Schema::Result::Tree",
  { tree_id => "tree_id" },
  { is_deferrable => 1, on_delete => "RESTRICT", on_update => "RESTRICT" },
);

=head2 trees

Type: many_to_many

Composing rels: L</root_nodes> -> tree

=cut

__PACKAGE__->many_to_many("trees", "root_nodes", "tree");


# Created by DBIx::Class::Schema::Loader v0.07040 @ 2019-07-10 07:23:16
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Wn78c8moYAEzLveg9aG6GA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
